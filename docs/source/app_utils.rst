app\_utils package
==================

Module contents
---------------

.. automodule:: app_utils
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

app\_utils.settings module
--------------------------

.. automodule:: app_utils.settings
   :members:
   :undoc-members:
   :show-inheritance:



