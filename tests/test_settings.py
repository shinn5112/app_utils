import sys
import os
import shutil
import unittest

# get my libs on the path for testing
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

from app_utils.settings import *

app_name = "test"
path = ""
default_settings_json = dict()
default_settings_ini = dict()

# json settings test data
default_settings_json["test"] = "test"
default_settings_json["new"] = "test"
default_settings_json["new2"] = "test"

# ini settings usage data
default_settings_ini["section1"] = dict()
default_settings_ini["section1"]["test"] = 1
default_settings_ini["section1"]["test2"] = 1
default_settings_ini["section2"] = dict()
default_settings_ini["section2"]["test"] = 1
default_settings_ini["empty section"] = dict()

# bad settings for ini
default_settings_ini_bad = dict()
default_settings_ini_bad["pass me"] = dict()
default_settings_ini_bad["fail me"] = 1


def cleanup(path: str):
    clean_dir = path.split('/')
    clean_dir = clean_dir[0:len(clean_dir) - 1]
    clean_dir = '/'.join(clean_dir)
    shutil.rmtree(clean_dir)


class TestSettingsJSON(unittest.TestCase):
    """
    Test cases for the SettingsJSON class.
    """

    def test_json_settings_constructor(self):
        """
        Tests that constructor generates the settings file

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        assert settings.settings == settings.default_settings
        assert os.path.exists(settings.path)
        cleanup(settings.path)

    def test_json_settings_constructor_load_from_file(self):
        """
        Tests that the constructor will load from file if file exists

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)  # build file
        settings = SettingsJSON(default_settings_json, app_name)  # recall, but should load from file, used for coverage
        assert settings.settings == settings.default_settings  # we didn't change anything. so this should be true.
        cleanup(settings.path)

    def test_json_settings_get_json_dict(self):
        """
        Tests that the get_json_dict returns the proper dictionary.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        assert settings.get_json_dict() == settings.settings
        cleanup(settings.path)

    def test_json_settings_get_setting(self):
        """
        Tests that getting an individual setting works.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        assert settings.get_setting("test") == settings.settings["test"]
        cleanup(settings.path)

    def test_json_settings_set_setting(self):
        """
        Tests that setting an individual setting works.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        settings.set_setting("test", 1)
        assert settings.settings["test"] == 1
        cleanup(settings.path)

    def test_json_settings_reset_setting(self):
        """
        Tests that resetting an individual setting works.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        settings.set_setting("test", 1)
        assert settings.settings["test"] == 1
        settings.reset_setting("test")
        assert settings.settings["test"] == settings.default_settings["test"]
        cleanup(settings.path)

    def test_json_settings_reset_all(self):
        """
        Tests that resetting all settings works.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        settings.set_setting("test", 1)
        settings.set_setting("new", 1)
        settings.set_setting("new2", 1)
        assert settings.settings["test"] == 1
        assert settings.settings["new"] == 1
        assert settings.settings["new2"] == 1
        settings.reset_all()
        assert settings.settings == settings.default_settings
        cleanup(settings.path)

    def test_json_settings_set_all(self):
        """
        Tests that replacing the json dictionary works.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)
        settings.set_setting("test", 1)
        settings.set_setting("new", 1)
        settings.set_setting("new2", 1)
        assert settings.settings["test"] == 1
        assert settings.settings["new"] == 1
        assert settings.settings["new2"] == 1
        settings.set_all(settings.default_settings)  # effectively a reset
        assert settings.settings == settings.default_settings
        cleanup(settings.path)

    def test_json_settings_constructor_new_defaults_added(self):
        """
        Tests that adding new default settings will not overwrite the user's settings that exist
        and that the new setting is indeed added.

        :return: None
        """
        settings = SettingsJSON(default_settings_json, app_name)  # build file
        settings.set_setting("new", 12)  # write settings change
        default_settings_json["new key"] = "12"
        settings = SettingsJSON(default_settings_json, app_name)  # recall, but should load from file, used for coverage
        assert settings.get_setting("new") == 12  # value was not overwritten by defaults
        assert settings.get_setting("new key") == "12"  # new default was added
        cleanup(settings.path)


class TestSettingsINI(unittest.TestCase):
    """
    Test cases for SettingsINI class.
    """

    def test_ini_settings_constructor(self):
        """
        Ensures that the constructor generates the settings file.

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        global path
        path = settings.path
        valid = True
        for section in settings.default_settings:
            if section not in settings.settings.sections():
                valid = False  # pragma: no cover
                break  # pragma: no cover
            for item in settings.default_settings[section]:
                if settings.settings[section][item] != str(settings.default_settings[section][item]):
                    valid = False  # pragma: no cover
                    break  # pragma: no cover
            if not valid:
                break  # pragma: no cover
        assert valid
        assert os.path.exists(settings.path)
        cleanup(settings.path)

    def test_ini_settings_constructor_load_from_file(self):
        """
        Tests that the constructor loads from a file if the files already exists.

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)  # builds settings file
        settings = SettingsINI(default_settings_ini, app_name)  # recall, but should load from file, used for coverage
        valid = True
        for section in settings.default_settings:
            if section not in settings.settings.sections():
                valid = False  # pragma: no cover
                break  # pragma: no cover
            for item in settings.default_settings[section]:
                if settings.settings[section][item] != str(settings.default_settings[section][item]):
                    valid = False  # pragma: no cover
                    break  # pragma: no cover
            if not valid:
                break  # pragma: no cover
        assert valid
        assert os.path.exists(settings.path)
        cleanup(settings.path)

    def test_ini_settings_constructor_rejects_bad_dict(self):
        """
        Tests to ensure that a malformed dict is rejected.

        :return: None
        """
        with self.assertRaises(TypeError):
            settings = SettingsINI(default_settings_ini_bad, app_name)
        self.assertFalse(os.path.exists(path))

    def test_ini_settings_set_setting(self):
        """
        Tests to ensure that setting a individual setting works.

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        settings.set_setting("section1", "test", 10)
        assert settings.settings["section1"]["test"] == "10"
        cleanup(settings.path)

    def test_ini_setting_get_setting_rejects_bad_type(self):
        """
        Tests to ensure set setting only allows str, int, and float types

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        with self.assertRaises(TypeError):
            settings.get_setting("section1", "test", object())
        cleanup(settings.path)

    def test_ini_settings_reset_setting(self):
        """
        Tests to ensure that resetting an individual settings works.

        :return: None
        """
        # checking to ensure reset settings is working
        settings = SettingsINI(default_settings_ini, app_name)
        settings.set_setting("section1", "test", 10)
        assert settings.settings["section1"]["test"] == "10"  # redundant assertion, but they make me feel safe.
        settings.reset_setting("section1", "test")
        assert settings.settings["section1"]["test"] == str(settings.default_settings["section1"]["test"])
        cleanup(settings.path)

    def test_ini_settings_reset_section(self):
        """
        Tests to ensure that resetting an entire section works.

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        settings.set_setting("section1", "test", 10)
        settings.set_setting("section1", "test2", 10)
        # redundant assertions, but they make me feel safe.
        assert settings.settings["section1"]["test"] == "10"
        assert settings.settings["section1"]["test2"] == "10"
        settings.reset_section("section1")
        for item in settings.default_settings["section1"]:
            assert settings.settings["section1"][item] == str(settings.default_settings["section1"][item])
        cleanup(settings.path)

    def test_ini_settings_reset_all(self):
        """
        Tests to ensure resetting all settings works.

        :return:  None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        settings.set_setting("section1", "test", 10)
        settings.set_setting("section1", "test2", 10)
        settings.set_setting("section2", "test", 10)
        # redundant assertions, but they make me feel safe.
        assert settings.settings["section1"]["test"] == "10"
        assert settings.settings["section1"]["test2"] == "10"
        assert settings.settings["section2"]["test"] == "10"
        settings.reset_all()
        valid = True
        for section in settings.default_settings:
            if section not in settings.settings.sections():
                valid = False  # pragma: no cover
                break  # pragma: no cover
            for item in settings.default_settings[section]:
                if settings.settings[section][item] != str(settings.default_settings[section][item]):
                    valid = False  # pragma: no cover
                    break  # pragma: no cover
            if not valid:
                break  # pragma: no cover
        assert valid
        cleanup(settings.path)

    def test_ini_setting_get_setting(self):
        """
        Tests to ensure that getting a single setting works.

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        settings.set_setting("section1", "test", 10)
        settings.set_setting("section1", "test2", 10.0)
        assert settings.settings["section1"]["test"] == "10"
        assert settings.settings["section1"]["test2"] == "10.0"
        assert settings.get_setting("section1", "test") == "10"
        assert settings.get_setting("section1", "test", int()) == 10  # type casting validation
        assert settings.get_setting("section1", "test2", float()) == 10.0  # type casting validation
        cleanup(settings.path)

    def test_ini_settings_constructor_new_defaults_added(self):
        """
        Tests that adding new default settings will not overwrite the user's settings that exist
        and that the new setting is indeed added.

        :return: None
        """
        settings = SettingsINI(default_settings_ini, app_name)
        settings.set_setting("section1", "test", 10)
        settings.set_setting("section1", "test2", 10.0)
        default_settings_ini["empty section"]["not empty"] = "12"
        default_settings_ini["new empty"] = dict()
        settings = SettingsINI(default_settings_ini, app_name)
        assert settings.get_setting("section1", "test", int()) == 10  # setting not overwritten.
        assert settings.get_setting("section1", "test2", float()) == 10.0  # setting not overwritten.
        assert settings.get_setting("empty section", "not empty") == "12"  # new setting added
        assert "new empty" in settings.settings.sections()  # new section added
        cleanup(settings.path)
